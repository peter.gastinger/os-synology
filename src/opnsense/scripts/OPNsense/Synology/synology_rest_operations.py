#!/usr/local/bin/python3

'''
Synology Remote operations
- status: get nas status
- shutdown: shutdown nas
- startvm: start vm
- stopvm: stop vm
- listvms: listvms
- getvm: get vm details

some documentation for the API:
- https://github.com/kwent/syno/tree/master/definitions/DSM/6.2/23739
- https://global.download.synology.com/download/Document/Software/DeveloperGuide/Package/FileStation/All/enu/Synology_File_Station_API_Guide.pdf
- https://global.download.synology.com/download/Document/Software/DeveloperGuide/Package/Virtualization/All/enu/Synology_Virtual_Machine_Manager_API_Guide.pdf

https://gitlab.com/peter.gastinger/os-synology.git
(c) Peter Gastinger

python3 /usr/local/opnsense/scripts/OPNsense/Synology/synology_rest_operations.py -h
usage: synology_rest_operations.py [-h] --operation
                                   {status,shutdown,startvm,stopvm,listvms,getvm}
                                   [--vm VM] [--debug] [--mock]

Synology NAS Arg parser

optional arguments:
  -h, --help            show this help message and exit
  --operation {status,shutdown,startvm,stopvm,listvms,getvm}
                        Possible choices
  --vm VM               Specify name of VM to start or stop or get details
  --debug               Set logging level to debug and log to
                        /var/log/synology.log
  --mock                Mock requests to NAS, do not send any real requests at
                        all

'''

import json
import os
import sys

import requests

requests.packages.urllib3.disable_warnings()
import logging
import argparse
import socket

from configparser import ConfigParser

socket.setdefaulttimeout(10)

base_url = 'https://{nas_ip}:5001/webapi/'
login_url = (
        base_url
        + 'auth.cgi?api=SYNO.API.Auth&version=3&method=login&account={username}&passwd={password}&session=FileStation&format=cookie'
)
logout_url = (
        base_url + 'auth.cgi?api=SYNO.API.Auth&version=1&method=logout&session=FileStation'
)
status_url = base_url + 'entry.cgi?api=SYNO.Core.System.Status&version=1&method=get'
shutdown_url = base_url + 'entry.cgi?api=SYNO.Core.System&version=1&method=shutdown'
poweron_vm_url = base_url + 'entry.cgi?api=SYNO.Virtualization.API.Guest.Action&version=1&method=poweron&guest_name={vm_name}'
shutdown_vm_url = base_url + 'entry.cgi?api=SYNO.Virtualization.API.Guest.Action&version=1&method=shutdown&guest_name={vm_name}'
list_vm_url = base_url + 'entry.cgi?api=SYNO.Virtualization.API.Guest&version=1&method=list'
get_vm_url = base_url + 'entry.cgi?api=SYNO.Virtualization.API.Guest&version=1&method=get&guest_name={vm_name}'


nas_operation_mapping_to_url = {'shutdown': shutdown_url, 'status': status_url, 'startvm':poweron_vm_url, 'stopvm':shutdown_vm_url, 'listvms':list_vm_url, 'getvm':get_vm_url}


def nas_login(nas_ip, username, password):
    session = requests.Session()
    # TODO does not work when run via configd, import of certificate ca necessary!
    session.verify = False
    if args.mock:
        logging.debug('Mock request')
        with requests_mock.Mocker() as m:
            m.get(login_url.format(nas_ip=nas_ip, username=username, password=password),
                  json={'data': {'sid': 'dummy-sid'}, 'success': True})
            res = session.get(
                login_url.format(nas_ip=nas_ip, username=username, password=password)
            )
    else:
        res = session.get(
            login_url.format(nas_ip=nas_ip, username=username, password=password)
        )
    res.raise_for_status()
    logging.debug('Login')
    logging.debug(res.json())
    return session


def nas_logout(session, nas_ip):
    if session:
        if args.mock:
            logging.debug('Mock request')
            with requests_mock.Mocker() as m:
                m.get(logout_url.format(nas_ip=nas_ip), json={'success': True})
                res = session.get(logout_url.format(nas_ip=nas_ip))
        else:
            res = session.get(logout_url.format(nas_ip=nas_ip))
        res.raise_for_status()
        logging.debug('Logout')
        logging.debug(res.json())
    else:
        logging.error('Invalid session')


def nas_do_request(nas_ip, username, password, url):
    logging.debug(f'url: {url}')
    session = nas_login(nas_ip, username, password)
    try:
        if args.mock:
            logging.debug('Mock request')
            with requests_mock.Mocker() as m:
                m.get(url,
                      json={'data': {'is_system_crashed': False, 'upgrade_ready': False}, 'success': True})
                res = session.get(url)
        else:
            res = session.get(url)
        res.raise_for_status()
        return res.json()
    except Exception as error:
        logging.error(error)
        raise
    finally:
        nas_logout(session, nas_ip)


def nas_operation(operation, vm=''):
    result = {}
    if os.path.exists(synology_config):
        cnf = ConfigParser()
        cnf.read(synology_config)
        if cnf.has_section('general'):
            try:
                nas_ip = cnf.get('general', 'ip')
                username = cnf.get('general', 'username')
                password = cnf.get('general', 'password')
                url =  nas_operation_mapping_to_url[operation]
                url_args = {'nas_ip':nas_ip}
                if operation in ["startvm", "stopvm", "getvm"]:
                    url_args.update({'vm_name':vm})
                result['message'] = nas_do_request(
                    nas_ip, username, password, url.format(**url_args)
                )
            except socket.error as error:
                logging.error(error)
                if error.strerror is None:
                    # probably hit timeout
                    result['message'] = 'time out!'
                else:
                    result['message'] = error.strerror
            except Exception as error:
                logging.error(error)
                result['message'] = '%s' % error
        else:
            # empty config
            logging.error('empty configuration')
            result['message'] = 'empty configuration'
    else:
        # no config
        logging.error('no configuration file found')
        result['message'] = 'no configuration file found'
    return result


if __name__ == '__main__':
    synology_config = '/usr/local/etc/synology/synology.conf'
    logfile = '/var/log/synology.log'
    parser = argparse.ArgumentParser(description='Synology NAS Arg parser')
    parser.add_argument(
        '--operation',
        choices=['status', 'shutdown', 'startvm', 'stopvm', 'listvms', 'getvm'],
        help='Possible choices',
        required=True,
    )
    parser.add_argument(
        '--vm',
        help='Specify name of VM to start or stop or get details',
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help=f'Set logging level to debug and log to {logfile}',
    )
    parser.add_argument(
        '--mock',
        action='store_true',
        help='Mock requests to NAS, do not send any real requests at all',
    )
    args = parser.parse_args()

    loglevel = logging.INFO
    if args.debug:
        loglevel = logging.DEBUG

    if args.mock:
        loglevel = logging.DEBUG
        try:
            import requests_mock
        except ModuleNotFoundError:
            logging.error(
                'Python requests-mock not found, please download it manually and install it using easy_install-3.7. Not recommended for prod setups!')
            sys.exit(-1)

    logging.basicConfig(
        filename=logfile,
        level=loglevel,
        format='%(asctime)s %(message)s',
    )

    result = nas_operation(args.operation, args.vm)
    logging.info(result)
    print(json.dumps(result))
    logging.debug('Done')

