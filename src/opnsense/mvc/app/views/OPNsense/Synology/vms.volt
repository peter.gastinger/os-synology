{#

OPNsense® is Copyright © 2014 – 2015 by Deciso B.V.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

#}

<script>
    $( document ).ready(function() {
		ajaxCall(url="/api/synology/service/listvms", sendData={},callback=function(data,status) {
			var trHTML = '';
					guests = data["message"]["data"]["guests"]
				$.each(guests, function (i, item) {
					trHTML += '<tr><td>' + item.guest_id + '</td><td>' + item.guest_name + '</td><td>' + item.status + '</td>'
					if (item.status == "running") {
						trHTML += '<td><button class="btn btn-primary shutdownvm" value="'+item.guest_name+'">Shutdown</button></td>';
					} else {
						trHTML += '<td><button class="btn btn-secondary startvm" value="'+item.guest_name+'">Start</button></td>';
						}
					trHTML += '</tr>\n';
				});
				$('#vm_list_body').append(trHTML);
			$(".shutdownvm").click(function() {
						vm = $(this).attr("value");
					console.log("stop vm "+vm);
					$("#responseMsg").removeClass("hidden");
					ajaxCall(url="/api/synology/service/stopvm", sendData={'vm':vm},callback=function(data,status) {
						console.log(data);
						$("#responseMsg").text(JSON.stringify(data));
					});
			});
			$(".startvm").click(function() {
						vm = $(this).attr("value");
					console.log("start vm "+vm);
					$("#responseMsg").removeClass("hidden");
					ajaxCall(url="/api/synology/service/startvm", sendData={'vm':vm},callback=function(data,status) {
						console.log(data);
						$("#responseMsg").text(JSON.stringify(data));
						});
			});
		});
    });
</script>

<div class="alert alert-info hidden" role="alert" id="responseMsg">
</div>

<div class="content-box">
<table id="vm_list" class="table table-condensed table-hover table-striped">
    <thead>
        <tr>
            <th data-column-id="id" data-type="string" data-identifier="true"  data-visible="false">{{ lang._('ID') }}</th>
            <th data-column-id="name" data-width="6em" data-type="string" data-formatter="rowtoggle">{{ lang._('Name') }}</th>
            <th data-column-id="status" data-type="string">{{ lang._('Status') }}</th>
            <th data-column-id="action" data-type="string">{{ lang._('Action') }}</th>
        </tr>
    </thead>
    <tbody id="vm_list_body">
    </tbody>
    <tfoot>
    </tfoot>
</table>
</div>
