<?php

/**
 *    Copyright (C) 2015 Deciso B.V.
 *
 *    All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 *    AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *    AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 *    OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *    POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace OPNsense\Synology\Api;

use OPNsense\Base\ApiControllerBase;
use OPNsense\Core\Backend;
use Phalcon\Filter\FilterFactory;


/**
 * Class ServiceController
 * @package OPNsense\Cron
 */
class ServiceController extends ApiControllerBase
{

    /**
     * reconfigure Synology
     */
    public function reloadAction()
    {
        $status = "failed";
        if ($this->request->isPost()) {
            $backend = new Backend();
            $bckresult = trim($backend->configdRun('template reload OPNsense/Synology'));
            if ($bckresult == "OK") {
                $status = "ok";
            }
        }
        return array("status" => $status);
    }

    /**
     * test Synology
     */
    public function shutdownAction()
    {
        if ($this->request->isPost()) {
            $backend = new Backend();
            $bckresult = json_decode(trim($backend->configdRun("synology shutdown")), true);
            if ($bckresult !== null) {
                // only return valid json type responses
                return $bckresult;
            }
            return array("message" => "got no result");
        }
        return array("message" => "unable to run config action");
    }

    /**
     * test Synology
     */
    public function statusAction()
    {
        if ($this->request->isPost()) {
            $backend = new Backend();
            $bckresult = json_decode(trim($backend->configdRun("synology status")), true);
            if ($bckresult !== null) {
                // only return valid json type responses
                return $bckresult;
            }
            return array("message" => "got no result");
        }
        return array("message" => "unable to run config action");
    }

      /**
       * start Synology vm
       */
      public function startvmAction()
      {
          $filter = (new FilterFactory())->newInstance();
          if ($this->request->isPost()) {
              $backend = new Backend();
              if ($this->request->hasPost('vm')) {
                  $vm = $filter->sanitize($this->request->getPost('vm'), "string");
                  $bckresult = json_decode(trim($backend->configdRun("synology startvm $vm")), true);
                  if ($bckresult !== null) {
                      // only return valid json type responses
                      return $bckresult;
                  }
                  return array("message" => "got no result");
              }
          }
          return array("message" => "unable to run config action");
      }
       
      /**
       * stop Synology vm
       */
      public function stopvmAction()
      {

          $filter = (new FilterFactory())->newInstance();
          if ($this->request->isPost()) {
              $backend = new Backend();
              if ($this->request->hasPost('vm')) {
                  $vm = $filter->sanitize($this->request->getPost('vm'), "string");
                  $bckresult = json_decode(trim($backend->configdRun("synology stopvm $vm")), true);
                  if ($bckresult !== null) {
                      // only return valid json type responses
                      return $bckresult;
                  }
                  return array("message" => "got no result");
              }
          }
          return array("message" => "unable to run config action");
      }

      /**
       * get Synology vm
       */
      public function getvmAction()
      {

          $filter = (new FilterFactory())->newInstance();
          if ($this->request->isPost()) {
              $backend = new Backend();
              if ($this->request->hasPost('vm')) {
                  $vm = $filter->sanitize($this->request->getPost('vm'), "string");
                  $bckresult = json_decode(trim($backend->configdRun("synology getvm $vm")), true);
                  if ($bckresult !== null) {
                      // only return valid json type responses
                      return $bckresult;
                  }
                  return array("message" => "got no result");
              }
          }
          return array("message" => "unable to run config action");
      }

      /**
       * list Synology vms
       */
      public function listvmsAction()
      {

          if ($this->request->isPost()) {
              $backend = new Backend();
              $bckresult = json_decode(trim($backend->configdRun("synology listvms")), true);
              if ($bckresult !== null) {
                  // only return valid json type responses
                  return $bckresult;
              }
              return array("message" => "got no result");
          }
          return array("message" => "unable to run config action");
      }
  }


