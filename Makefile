PLUGIN_NAME=		synology
PLUGIN_VERSION=		0.7
PLUGIN_REVISION=	1
PLUGIN_COMMENT=		Synology NAS and VM operations (e.g. shutdown)
PLUGIN_MAINTAINER=	peter.gastinger@gmail.com

.include "../../Mk/plugins.mk"
