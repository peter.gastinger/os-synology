#!/usr/bin/env python3

# shutdown nas via opnsense rest api

import requests
from pprint import pprint

api_key='apikey_for_user'
api_secret='apisecret_for_user'
shutdown_url = 'https://192.168.20.38/api/synology/service/shutdown'

session = requests.Session()
session.auth = (api_key, api_secret)

res = session.post(shutdown_url)
pprint(res.json())
