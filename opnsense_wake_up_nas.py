#!/usr/bin/env python3

# wake up nas via opnsense rest api for wake on lan

import requests
from pprint import pprint

api_key='apikey_for_user'
api_secret='apisecret_for_user'
wol_url = 'https://192.168.20.38/api/wol/wol/set'

session = requests.Session()
session.auth = (api_key, api_secret)

res = session.post(wol_url, json={'uuid':'3cd3e5fc-c586-4767-993a-2366c0e50796'})
pprint(res.json())
